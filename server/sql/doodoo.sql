-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 2018-09-07 15:52:06
-- 服务器版本： 5.6.38
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `doodoo`
--

-- --------------------------------------------------------

--
-- 表的结构 `admin`
--

CREATE TABLE `admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `avater_id` int(11) NOT NULL DEFAULT '0',
  `nickname` text,
  `password` text,
  `phone` text COMMENT '手机号',
  `email` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `admin`
--

INSERT INTO `admin` (`id`, `avater_id`, `nickname`, `password`, `phone`, `email`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 'admin', '21232f297a57a5a743894a0e4a801fc3', '18538253627', '1604583867@qq.com', 1, '2016-04-06 02:34:31', '2018-08-13 07:32:00', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `admin_log`
--

CREATE TABLE `admin_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(11) NOT NULL DEFAULT '0' COMMENT '超管',
  `ip` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ip',
  `info` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '信息',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='超管日志';

--
-- 转存表中的数据 `admin_log`
--

INSERT INTO `admin_log` (`id`, `admin_id`, `ip`, `info`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '::ffff:127.0.0.1', '登录', '2018-09-05 02:06:18', '2018-09-05 02:06:18', NULL),
(2, 1, '::ffff:127.0.0.1', '登录', '2018-09-06 07:01:44', '2018-09-06 07:01:44', NULL),
(3, 1, '::ffff:127.0.0.1', '登录', '2018-09-06 07:03:41', '2018-09-06 07:03:41', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `ads`
--

CREATE TABLE `ads` (
  `id` int(11) UNSIGNED NOT NULL,
  `img_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '@must@upload图片链接',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '简介',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '跳转',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:登录页广告,2:营销应用页广告,3:内容管理首页广告,4:内容管理页右侧广告',
  `rank` tinyint(4) NOT NULL DEFAULT '0' COMMENT '排序',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='广告';

--
-- 转存表中的数据 `ads`
--

INSERT INTO `ads` (`id`, `img_url`, `name`, `info`, `url`, `status`, `type`, `rank`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'http://img.doodooke.qingful.com/	\r\ndoodooke-swiper1.jpg', NULL, NULL, '/', 1, 1, 0, NULL, NULL, NULL),
(3, 'http://img.doodooke.qingful.com/7acc38fa-6d2d-4fe5-b3fa-8f4cec546627.jpg', '多多客公测', '多多客公测', '/public/login', 1, 3, 0, NULL, NULL, NULL),
(4, 'http://img.doodooke.qingful.com/038d21c7-8e93-4fbf-9319-442d7fb0b7e4.jpg', '缤纷八月 多多有礼', '缤纷八月 多多有礼', 'http://www.doodooke.com', 1, 3, 10, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `analysis_daily_summary`
--

CREATE TABLE `analysis_daily_summary` (
  `id` int(10) UNSIGNED NOT NULL,
  `ref_date` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `custom` int(11) NOT NULL DEFAULT '0',
  `app` int(11) NOT NULL DEFAULT '0',
  `wxa` int(11) NOT NULL DEFAULT '0',
  `user` int(11) NOT NULL DEFAULT '0',
  `trade` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `analysis_daily_summary`
--

INSERT INTO `analysis_daily_summary` (`id`, `ref_date`, `custom`, `app`, `wxa`, `user`, `trade`, `created_at`, `updated_at`) VALUES
(1, '20180905', 0, 1, 0, 0, 0, '2018-09-05 02:39:48', '2018-09-05 02:39:48');

-- --------------------------------------------------------

--
-- 表的结构 `analysis_product_effect_summary`
--

CREATE TABLE `analysis_product_effect_summary` (
  `id` int(10) UNSIGNED NOT NULL,
  `wxa_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品id',
  `visitor` int(11) NOT NULL DEFAULT '0' COMMENT '商品访客数',
  `view` int(11) NOT NULL DEFAULT '0' COMMENT '商品浏览数',
  `trade` int(11) NOT NULL DEFAULT '0' COMMENT '付款人数',
  `ref_date` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `analysis_product_summary`
--

CREATE TABLE `analysis_product_summary` (
  `id` int(10) UNSIGNED NOT NULL,
  `wxa_id` int(11) NOT NULL DEFAULT '0',
  `visit` int(11) NOT NULL DEFAULT '0' COMMENT '被访问商品数',
  `visitor` int(11) NOT NULL DEFAULT '0' COMMENT '商品访客数',
  `view` int(11) NOT NULL DEFAULT '0' COMMENT '商品浏览数',
  `ref_date` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `analysis_product_user_summary`
--

CREATE TABLE `analysis_product_user_summary` (
  `id` int(10) UNSIGNED NOT NULL,
  `wxa_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0' COMMENT '被访问商品id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品访客id',
  `ref_date` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `analysis_wxa_daily_summary`
--

CREATE TABLE `analysis_wxa_daily_summary` (
  `id` int(10) UNSIGNED NOT NULL,
  `wxa_id` int(11) NOT NULL DEFAULT '0',
  `ref_date` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `user` int(11) NOT NULL DEFAULT '0',
  `trade` int(11) NOT NULL DEFAULT '0',
  `visitor` int(11) NOT NULL DEFAULT '0' COMMENT '访客数',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `app`
--

CREATE TABLE `app` (
  `id` int(11) UNSIGNED NOT NULL,
  `custom_id` int(11) NOT NULL DEFAULT '0' COMMENT '客户',
  `template_id` int(11) NOT NULL DEFAULT '0' COMMENT '模版',
  `template_wxa_id` int(11) NOT NULL DEFAULT '0' COMMENT '小程序模版',
  `template_industry_id` int(11) NOT NULL DEFAULT '0' COMMENT '行业',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '介绍',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `started_at` timestamp NULL DEFAULT NULL COMMENT '@dateTimePicker开始时间',
  `ended_at` timestamp NULL DEFAULT NULL COMMENT '@dateTimePicker结束时间',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='应用';

--
-- 转存表中的数据 `app`
--

INSERT INTO `app` (`id`, `custom_id`, `template_id`, `template_wxa_id`, `template_industry_id`, `name`, `info`, `status`, `started_at`, `ended_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 10, 24, '新零售demo', 'demo', 1, '2018-09-05 02:39:47', '2018-09-12 02:39:47', '2018-09-05 02:39:48', '2018-09-05 02:48:56', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `app_address`
--

CREATE TABLE `app_address` (
  `id` int(10) UNSIGNED NOT NULL,
  `app_id` int(11) NOT NULL DEFAULT '0' COMMENT '应用',
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省',
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '市',
  `district` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '区',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='应用地址';

--
-- 转存表中的数据 `app_address`
--

INSERT INTO `app_address` (`id`, `app_id`, `province`, `city`, `district`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '北京市', '市辖区', '东城区', '2018-09-05 02:39:48', '2018-09-05 02:39:48', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `app_log`
--

CREATE TABLE `app_log` (
  `id` int(11) UNSIGNED NOT NULL,
  `app_id` int(11) NOT NULL DEFAULT '0',
  `info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='应用日志';

-- --------------------------------------------------------

--
-- 表的结构 `app_trade`
--

CREATE TABLE `app_trade` (
  `id` int(10) UNSIGNED NOT NULL,
  `app_id` int(11) NOT NULL DEFAULT '0' COMMENT 'app_id',
  `custom_id` int(11) NOT NULL DEFAULT '0' COMMENT '客户id',
  `tradeid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '交易单号',
  `money` float NOT NULL DEFAULT '0' COMMENT '金额',
  `payment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '支付方式',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '支付状态',
  `years` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0表示续费交易',
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='交易';

--
-- 转存表中的数据 `app_trade`
--

INSERT INTO `app_trade` (`id`, `app_id`, `custom_id`, `tradeid`, `money`, `payment`, `status`, `years`, `type`, `remark`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '20180905104740', 1999, '支付宝', 0, 1, 0, NULL, '2018-09-05 02:47:41', '2018-09-05 02:47:41', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `app_ump`
--

CREATE TABLE `app_ump` (
  `id` int(11) UNSIGNED NOT NULL,
  `app_id` int(11) NOT NULL DEFAULT '0' COMMENT 'app_id',
  `ump_id` int(11) NOT NULL DEFAULT '0' COMMENT 'ump_id',
  `started_at` timestamp NULL DEFAULT NULL COMMENT '@dateTimePicker开始时间',
  `ended_at` timestamp NULL DEFAULT NULL COMMENT '@dateTimePicker结束时间',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='营销';

-- --------------------------------------------------------

--
-- 表的结构 `component_access_token`
--

CREATE TABLE `component_access_token` (
  `id` int(10) UNSIGNED NOT NULL,
  `component_access_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'component_access_token',
  `expires_in` int(11) NOT NULL DEFAULT '0' COMMENT '有效期',
  `expires_at` bigint(20) NOT NULL DEFAULT '0' COMMENT '失效时间',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='@hidden微信开放平台 component_access_token';

--
-- 转存表中的数据 `component_access_token`
--

INSERT INTO `component_access_token` (`id`, `component_access_token`, `expires_in`, `expires_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', 7200, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `component_verify_ticket`
--

CREATE TABLE `component_verify_ticket` (
  `id` int(10) UNSIGNED NOT NULL,
  `AppId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'appid',
  `CreateTime` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `InfoType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '类型',
  `ComponentVerifyTicket` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ComponentVerifyTicket',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='@hidden微信开放平台 component_verify_ticket';

--
-- 转存表中的数据 `component_verify_ticket`
--

INSERT INTO `component_verify_ticket` (`id`, `AppId`, `CreateTime`, `InfoType`, `ComponentVerifyTicket`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', 0, 'component_verify_ticket', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `custom`
--

CREATE TABLE `custom` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(11) NOT NULL DEFAULT '1' COMMENT '角色',
  `avater_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '头像',
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机号',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '密码',
  `password_org` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '原始密码',
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '昵称',
  `domain` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '注册域名',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '邮箱',
  `qq` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'qq',
  `sex` int(11) NOT NULL DEFAULT '0' COMMENT '0:男，1:女',
  `signature` text COLLATE utf8mb4_unicode_ci COMMENT '签名',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='客户表';

--
-- 转存表中的数据 `custom`
--

INSERT INTO `custom` (`id`, `group_id`, `avater_url`, `phone`, `password`, `password_org`, `nickname`, `domain`, `email`, `qq`, `sex`, `signature`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'http://img.doodooke.qingful.com/f7bc8a33-4f57-45a2-a276-067462af6885.jpg', '18538253627', 'e970cf317165c18bd982e95d08d4a013', '18538253627', '18538253627', 'doodooke.com', '786699892@qq.com', '786699892', 0, '多多客', 1, NULL, '2018-09-05 02:33:01', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `custom_group`
--

CREATE TABLE `custom_group` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 转存表中的数据 `custom_group`
--

INSERT INTO `custom_group` (`id`, `title`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '商家', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `custom_log`
--

CREATE TABLE `custom_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `custom_id` int(11) NOT NULL DEFAULT '0' COMMENT '客户',
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ip',
  `info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '信息',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='客户日志';

--
-- 转存表中的数据 `custom_log`
--

INSERT INTO `custom_log` (`id`, `custom_id`, `ip`, `info`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '::ffff:127.0.0.1', '登录', '2018-09-05 02:31:36', '2018-09-05 02:31:36', NULL),
(2, 1, '::ffff:127.0.0.1', '登录', '2018-09-06 06:56:46', '2018-09-06 06:56:46', NULL),
(3, 1, '::ffff:127.0.0.1', '登录', '2018-09-06 06:58:44', '2018-09-06 06:58:44', NULL),
(4, 1, '::ffff:127.0.0.1', '登录', '2018-09-06 07:03:25', '2018-09-06 07:03:25', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `file`
--

CREATE TABLE `file` (
  `id` int(10) UNSIGNED NOT NULL,
  `custom_id` int(11) NOT NULL DEFAULT '0' COMMENT '客户',
  `app_id` int(11) NOT NULL DEFAULT '0' COMMENT '应用',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '图片链接',
  `url_org` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `ext` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '扩展',
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '类型',
  `size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '大小',
  `savename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '存储文件名',
  `savepath` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '存储文件路径',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='文件';

--
-- 转存表中的数据 `file`
--

INSERT INTO `file` (`id`, `custom_id`, `app_id`, `url`, `url_org`, `name`, `ext`, `type`, `size`, `savename`, `savepath`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 0, 'http://127.0.0.1:3001/uploads/2018-09-05/dcc0a804-f7e1-45e7-ac98-a84317b49bef.jpg', 'http://127.0.0.1:3001/uploads/2018-09-05/dcc0a804-f7e1-45e7-ac98-a84317b49bef.jpg', '3264d4b8-c4fd-4995-b246-f5ea5a91c7c5.jpg', 'jpg', 'image/jpeg', '72265', 'dcc0a804-f7e1-45e7-ac98-a84317b49bef.jpg', '2018-09-05', '2018-09-05 03:51:03', '2018-09-05 03:51:03', NULL),
(2, 1, 0, 'http://127.0.0.1:3001/uploads/2018-09-05/cf3dd319-2803-4836-b323-1dab9034f55e.jpg', 'http://127.0.0.1:3001/uploads/2018-09-05/cf3dd319-2803-4836-b323-1dab9034f55e.jpg', '76b1e735-7146-45b9-b2fe-a72dd83db053.jpg', 'jpg', 'image/jpeg', '73776', 'cf3dd319-2803-4836-b323-1dab9034f55e.jpg', '2018-09-05', '2018-09-05 10:27:04', '2018-09-05 10:27:04', NULL),
(3, 1, 0, 'http://127.0.0.1:3001/uploads/2018-09-05/c8ab5458-71d0-422b-9201-e3c42a233e76.jpg', 'http://127.0.0.1:3001/uploads/2018-09-05/c8ab5458-71d0-422b-9201-e3c42a233e76.jpg', '8f6317ee-c6a1-4f38-b293-6d5d887e3904.jpg', 'jpg', 'image/jpeg', '60307', 'c8ab5458-71d0-422b-9201-e3c42a233e76.jpg', '2018-09-05', '2018-09-05 10:28:29', '2018-09-05 10:28:29', NULL),
(4, 1, 0, 'http://127.0.0.1:3001/uploads/2018-09-05/33889191-e696-4b71-a81b-ad7b88465633.jpg', 'http://127.0.0.1:3001/uploads/2018-09-05/33889191-e696-4b71-a81b-ad7b88465633.jpg', '648ada32-dd08-422f-be67-1beeabdc44bf.jpg', 'jpg', 'image/jpeg', '70039', '33889191-e696-4b71-a81b-ad7b88465633.jpg', '2018-09-05', '2018-09-05 10:29:42', '2018-09-05 10:29:42', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `kuaidi`
--

CREATE TABLE `kuaidi` (
  `id` int(11) NOT NULL,
  `delivery_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_sort` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `shop`
--

CREATE TABLE `shop` (
  `id` int(10) UNSIGNED NOT NULL,
  `app_id` int(11) NOT NULL DEFAULT '0' COMMENT '应用',
  `img_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'logo',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '介绍',
  `tel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '电话',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '地址',
  `lng` decimal(10,7) DEFAULT NULL COMMENT '经度',
  `lat` decimal(10,7) DEFAULT NULL COMMENT '纬度',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='店铺';

--
-- 转存表中的数据 `shop`
--

INSERT INTO `shop` (`id`, `app_id`, `img_url`, `name`, `info`, `tel`, `address`, `lng`, `lat`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'http://127.0.0.1:3001/uploads/2018-09-05/dcc0a804-f7e1-45e7-ac98-a84317b49bef.jpg', '新零售demo', NULL, '18538253627', '河南省郑州市金水区丰庆路街道豫武康居花园', '113.6501100', '34.8322540', 1, '2018-09-05 02:39:48', '2018-09-05 03:58:36', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `shop_coupon`
--

CREATE TABLE `shop_coupon` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `price` float NOT NULL DEFAULT '0' COMMENT '优惠价格',
  `img_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '图片',
  `limit_price` float NOT NULL DEFAULT '0' COMMENT '限制条件',
  `limit_num` int(11) NOT NULL DEFAULT '0' COMMENT '限制数量',
  `num` int(11) NOT NULL DEFAULT '0' COMMENT '数量',
  `has_num` int(11) NOT NULL COMMENT '已领取张数',
  `started_at` timestamp NULL DEFAULT NULL COMMENT '开始时间',
  `ended_at` timestamp NULL DEFAULT NULL COMMENT '结束时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `shop_deliveryfee`
--

CREATE TABLE `shop_deliveryfee` (
  `id` int(11) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺id',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '模板名称',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0为按件计算，1为按重计算',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态1:开启0:关闭',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='运费名称表';

-- --------------------------------------------------------

--
-- 表的结构 `shop_deliveryfee_sku`
--

CREATE TABLE `shop_deliveryfee_sku` (
  `id` int(11) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0',
  `fee_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fee表的id',
  `frist` smallint(6) NOT NULL DEFAULT '0' COMMENT '首重或首件',
  `frist_price` float NOT NULL DEFAULT '0' COMMENT '首重或首件价格',
  `other` smallint(6) NOT NULL DEFAULT '0' COMMENT '续重或续件',
  `other_price` float NOT NULL DEFAULT '0' COMMENT '续重或续件价格',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:全国配送,1:部分配送',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='运费不同地区运费价格表';

-- --------------------------------------------------------

--
-- 表的结构 `shop_deliveryfee_sku_address`
--

CREATE TABLE `shop_deliveryfee_sku_address` (
  `id` int(11) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0',
  `deliveryfee_sku_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fee_sku表id',
  `deliveryfee_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fee表id',
  `region_id` int(11) NOT NULL DEFAULT '0' COMMENT 'region表中的id',
  `pid` mediumint(9) NOT NULL DEFAULT '0' COMMENT '父级id',
  `name` varchar(30) CHARACTER SET utf8 DEFAULT '' COMMENT '地址名称',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='运费模板的配送地址表';

-- --------------------------------------------------------

--
-- 表的结构 `shop_fullcut`
--

CREATE TABLE `shop_fullcut` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺',
  `price` float NOT NULL DEFAULT '0' COMMENT '优惠价格',
  `limit_price` float NOT NULL DEFAULT '0' COMMENT '限制价格',
  `free_delivery` tinyint(1) NOT NULL DEFAULT '0' COMMENT '免配送费',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `shop_kuaidi`
--

CREATE TABLE `shop_kuaidi` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺id',
  `delivery_id` int(11) NOT NULL DEFAULT '0' COMMENT '快递id',
  `delivery_code` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '快递公司代码',
  `delivery_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '快递名称',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:开启 0：关闭',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='店铺快递表';

-- --------------------------------------------------------

--
-- 表的结构 `shop_order`
--

CREATE TABLE `shop_order` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户',
  `orderid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单号',
  `trade_id` int(11) NOT NULL DEFAULT '0' COMMENT 'tradeid',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发货时间',
  `pay_time` timestamp NULL DEFAULT NULL COMMENT '付款时间',
  `finsh_time` timestamp NULL DEFAULT NULL COMMENT '订单确认完成时的时间',
  `finsh_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0表示七天内可退货，1超出时间不可退货',
  `totalprice` float NOT NULL DEFAULT '0' COMMENT '总价',
  `totalprice_org` float NOT NULL DEFAULT '0' COMMENT '原价',
  `pay_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '支付状态',
  `payment_id` int(11) NOT NULL DEFAULT '1' COMMENT '支付方式：0，余额 ，1，微信支付，2，支付宝，3货到付款',
  `check` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1审核通过，-1审核拒绝，2售后申请审核中，3.退款申请审核中',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型0表示普通订单，1表示分销订单',
  `send_type` int(11) NOT NULL DEFAULT '0' COMMENT '0:送货上门1:到店自提',
  `store_id` int(11) NOT NULL DEFAULT '0' COMMENT '自提门店',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态 0:待发货，1:已发货，2:已完成，3:已评价,4退款完成，5售后完成, -1:已关闭，-2:退款中，-3售后中',
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  `prepay_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'formid',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `shop_order_contact`
--

CREATE TABLE `shop_order_contact` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户',
  `order_id` int(11) NOT NULL DEFAULT '0' COMMENT '订单',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '姓名',
  `phone` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机号',
  `province` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省',
  `city` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '市',
  `district` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '区',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '地址',
  `nationalcode` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家码',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `shop_order_detail`
--

CREATE TABLE `shop_order_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户',
  `order_id` int(11) NOT NULL DEFAULT '0' COMMENT '订单id',
  `product_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '商品名称',
  `sku_id` int(11) NOT NULL DEFAULT '0' COMMENT 'skuid',
  `sku_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'sku名称',
  `num` smallint(6) NOT NULL DEFAULT '0' COMMENT '数量',
  `price` float NOT NULL DEFAULT '0' COMMENT '价格',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `shop_order_kuaidi`
--

CREATE TABLE `shop_order_kuaidi` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺id',
  `order_id` int(11) NOT NULL DEFAULT '0' COMMENT '订单id',
  `delivery_id` int(11) NOT NULL DEFAULT '0' COMMENT '快递id',
  `delivery_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '快递公司名称',
  `delivery_code` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '快递公司代码',
  `tracking_number` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '快递单号',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='订单快递表';

-- --------------------------------------------------------

--
-- 表的结构 `shop_order_pintuan`
--

CREATE TABLE `shop_order_pintuan` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺',
  `order_id` int(11) NOT NULL DEFAULT '0' COMMENT '订单id',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '团长id，0为团长',
  `discount` int(11) NOT NULL DEFAULT '0' COMMENT '拼团折扣',
  `need_num` int(11) NOT NULL DEFAULT '0' COMMENT '拼团需要人数',
  `is_free` int(11) NOT NULL DEFAULT '0' COMMENT '团长是否免单（0：否 1：是）',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '1:拼团中，2:拼团成功，3:拼团失败',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 表的结构 `shop_order_service`
--

CREATE TABLE `shop_order_service` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `service_id` int(11) NOT NULL DEFAULT '0' COMMENT '售后服务id',
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refuse` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '售后内容',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:售后 1：退款',
  `status` int(11) DEFAULT '0' COMMENT '1:通过 0：审核中 -1：拒绝',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `shop_order_verify`
--

CREATE TABLE `shop_order_verify` (
  `id` int(11) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0' COMMENT '订单id',
  `coupon_id` int(11) NOT NULL DEFAULT '0' COMMENT '优惠券id',
  `full_id` int(11) NOT NULL DEFAULT '0' COMMENT '满减id',
  `coupon_price` float(9,2) NOT NULL DEFAULT '0.00' COMMENT '优惠价格',
  `full_price` float(9,2) NOT NULL DEFAULT '0.00' COMMENT '满减价格',
  `fee_price` float(9,2) NOT NULL DEFAULT '0.00' COMMENT '运费',
  `fee_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0为全国统一价格，1为全国包邮',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='订单优惠满减';

-- --------------------------------------------------------

--
-- 表的结构 `shop_product`
--

CREATE TABLE `shop_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺',
  `img_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '图片',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `subname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '子名称',
  `info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '介绍',
  `price` float NOT NULL DEFAULT '0' COMMENT '价格',
  `price_org` float NOT NULL DEFAULT '0' COMMENT '原价',
  `detail` text COLLATE utf8mb4_unicode_ci COMMENT '详情',
  `fee_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0为全国统一价格，1为全国包邮',
  `fee_price` float(9,2) NOT NULL DEFAULT '0.00' COMMENT '运费',
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '单位',
  `rank` int(11) NOT NULL DEFAULT '0',
  `sku_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'sku状态',
  `sale` int(11) NOT NULL DEFAULT '0' COMMENT ' 销量',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '商品状态',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0普通商品，1分销商品',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `shop_product`
--

INSERT INTO `shop_product` (`id`, `shop_id`, `img_url`, `name`, `subname`, `info`, `price`, `price_org`, `detail`, `fee_type`, `fee_price`, `unit`, `rank`, `sku_status`, `sale`, `status`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'http://127.0.0.1:3001/uploads/2018-09-05/dcc0a804-f7e1-45e7-ac98-a84317b49bef.jpg', '四川吉禄芒果4个约300g/个', NULL, NULL, 0.01, 0, '<p><img src=\"https://img.alicdn.com/imgextra/i3/3015214310/TB230HUJAKWBuNjy1zjXXcOypXa_!!3015214310.jpg\"/><img src=\"https://img.alicdn.com/imgextra/i1/3015214310/TB2YXrzJGmWBuNjy1XaXXXCbXXa_!!3015214310.jpg\"/><img src=\"https://img.alicdn.com/imgextra/i4/3015214310/TB2DmQ5JpOWBuNjy0FiXXXFxVXa_!!3015214310.jpg\"/></p>', 0, 0.00, 'kg', 0, 0, 0, 1, 0, '2018-09-05 03:51:56', '2018-09-05 03:51:56', NULL),
(2, 1, 'http://127.0.0.1:3001/uploads/2018-09-05/cf3dd319-2803-4836-b323-1dab9034f55e.jpg', '山东栖霞优质红富士8个200g以上/个', NULL, NULL, 7, 0, '', 0, 0.00, 'kg', 0, 0, 0, 1, 0, '2018-09-05 10:27:26', '2018-09-05 10:27:26', NULL),
(3, 1, 'http://127.0.0.1:3001/uploads/2018-09-05/c8ab5458-71d0-422b-9201-e3c42a233e76.jpg', '上海南汇玉菇甜瓜1个900g以上/个', NULL, NULL, 12.9, 0, '', 0, 0.00, 'kg', 0, 0, 0, 1, 0, '2018-09-05 10:28:52', '2018-09-05 10:28:52', NULL),
(4, 1, 'http://127.0.0.1:3001/uploads/2018-09-05/33889191-e696-4b71-a81b-ad7b88465633.jpg', '广东桂味荔枝2.5kg', NULL, NULL, 100, 0, '', 0, 0.00, 'kg', 0, 0, 0, 1, 0, '2018-09-05 10:29:56', '2018-09-05 10:29:56', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `shop_productgroup`
--

CREATE TABLE `shop_productgroup` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '简介',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `shop_productgroup`
--

INSERT INTO `shop_productgroup` (`id`, `shop_id`, `name`, `info`, `status`, `rank`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '水果', NULL, 0, 0, '2018-09-05 02:57:27', '2018-09-05 02:57:27', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `shop_productgroup_product`
--

CREATE TABLE `shop_productgroup_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺',
  `product_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品',
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '分组',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `shop_productgroup_product`
--

INSERT INTO `shop_productgroup_product` (`id`, `shop_id`, `product_id`, `group_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, '2018-09-05 03:51:56', '2018-09-05 03:51:56', NULL),
(2, 1, 2, 1, '2018-09-05 10:27:27', '2018-09-05 10:27:27', NULL),
(3, 1, 3, 1, '2018-09-05 10:28:52', '2018-09-05 10:28:52', NULL),
(4, 1, 4, 1, '2018-09-05 10:29:56', '2018-09-05 10:29:56', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `shop_productlabel`
--

CREATE TABLE `shop_productlabel` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '介绍',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `shop_productlabel_product`
--

CREATE TABLE `shop_productlabel_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `label_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `shop_productsku`
--

CREATE TABLE `shop_productsku` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺',
  `product_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品',
  `sku_ids` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'sku ids',
  `sku_pids` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'sku pids',
  `price` float NOT NULL DEFAULT '0' COMMENT '价格',
  `price_org` float NOT NULL DEFAULT '0' COMMENT '原价',
  `store` int(11) NOT NULL DEFAULT '0' COMMENT '库存',
  `sale` int(11) NOT NULL DEFAULT '0' COMMENT '销量',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `shop_product_album`
--

CREATE TABLE `shop_product_album` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺',
  `product_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品',
  `img_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '图片',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `shop_product_album`
--

INSERT INTO `shop_product_album` (`id`, `shop_id`, `product_id`, `img_url`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'http://127.0.0.1:3001/uploads/2018-09-05/dcc0a804-f7e1-45e7-ac98-a84317b49bef.jpg', '2018-09-05 03:51:56', '2018-09-05 03:51:56', NULL),
(2, 1, 2, 'http://127.0.0.1:3001/uploads/2018-09-05/cf3dd319-2803-4836-b323-1dab9034f55e.jpg', '2018-09-05 10:27:27', '2018-09-05 10:27:27', NULL),
(3, 1, 3, 'http://127.0.0.1:3001/uploads/2018-09-05/c8ab5458-71d0-422b-9201-e3c42a233e76.jpg', '2018-09-05 10:28:52', '2018-09-05 10:28:52', NULL),
(4, 1, 4, 'http://127.0.0.1:3001/uploads/2018-09-05/33889191-e696-4b71-a81b-ad7b88465633.jpg', '2018-09-05 10:29:56', '2018-09-05 10:29:56', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `shop_product_pintuan_set`
--

CREATE TABLE `shop_product_pintuan_set` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `discount` int(11) DEFAULT '0' COMMENT '商品打折',
  `num` int(11) DEFAULT '0' COMMENT '参团人数',
  `time` int(11) DEFAULT '24' COMMENT '拼团时间',
  `is_free` int(11) NOT NULL DEFAULT '0' COMMENT '团长是否免单（0：否 1：是）',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 表的结构 `shop_region`
--

CREATE TABLE `shop_region` (
  `id` mediumint(6) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'id',
  `pid` mediumint(6) UNSIGNED NOT NULL DEFAULT '0' COMMENT '上级id',
  `name` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '名称',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型：0-省，1-市，2-县',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='全国行政区划表';

-- --------------------------------------------------------

--
-- 表的结构 `shop_service`
--

CREATE TABLE `shop_service` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0：售后 1：退款',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='售后类型';

--
-- 转存表中的数据 `shop_service`
--

INSERT INTO `shop_service` (`id`, `shop_id`, `name`, `type`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '不想要了', 0, 1, '2018-09-05 02:55:32', '2018-09-05 02:55:43', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `shop_sku`
--

CREATE TABLE `shop_sku` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '上级',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `shop_user_collection`
--

CREATE TABLE `shop_user_collection` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户',
  `collection_id` int(11) NOT NULL DEFAULT '0' COMMENT '收藏id',
  `collection_type` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收藏类型',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `shop_user_coupon`
--

CREATE TABLE `shop_user_coupon` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户',
  `coupon_id` int(11) NOT NULL DEFAULT '0' COMMENT '优惠券',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `template`
--

CREATE TABLE `template` (
  `id` int(10) UNSIGNED NOT NULL,
  `img_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '文件',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '主题色',
  `info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '介绍',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '链接',
  `price` float NOT NULL DEFAULT '0' COMMENT '价格：一年',
  `price_tag` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '价格标签',
  `old_price` float NOT NULL DEFAULT '0' COMMENT '原价：一年',
  `free` int(11) NOT NULL DEFAULT '100' COMMENT '每日免费',
  `open_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '开通链接',
  `info_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更多',
  `rank` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态',
  `online` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否上线',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='应用模版';

--
-- 转存表中的数据 `template`
--

INSERT INTO `template` (`id`, `img_url`, `name`, `color`, `info`, `url`, `price`, `price_tag`, `old_price`, `free`, `open_url`, `info_url`, `rank`, `status`, `online`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '/store.png', '新零售', '#ea3f33', '打造线上智能化零售平台，解决在线卖货，互联网营销，会员管理的解决方案。', '', 1999, NULL, 1999, 0, '/app/create', '', 9, 1, 1, NULL, '2018-04-20 10:18:49', NULL),
(2, '/wz.png', '新微站', '#9a26fd', '完全可视化拖拽，N+营销工具引流 海量优质行业模板，3步打造自己的小程序。', '', 999, NULL, 999, 0, '/app/create', '', 10, 0, 0, NULL, '2018-04-20 10:18:49', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `template_industry`
--

CREATE TABLE `template_industry` (
  `id` int(10) UNSIGNED NOT NULL,
  `template_id` int(11) NOT NULL DEFAULT '0' COMMENT '模版',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '行业名称',
  `rank` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='应用模版行业';

--
-- 转存表中的数据 `template_industry`
--

INSERT INTO `template_industry` (`id`, `template_id`, `name`, `rank`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, '企业', 0, NULL, NULL, NULL),
(2, 2, '婚庆', 0, NULL, NULL, NULL),
(3, 2, '汽车', 0, NULL, NULL, NULL),
(4, 2, '鲜花', 0, NULL, NULL, NULL),
(5, 2, '教育', 0, NULL, NULL, NULL),
(6, 2, '运动', 0, NULL, NULL, NULL),
(7, 2, '酒店', 0, NULL, NULL, NULL),
(8, 2, 'KTV', 0, NULL, NULL, NULL),
(9, 2, '金融', 0, NULL, NULL, NULL),
(10, 2, '家装', 0, NULL, NULL, NULL),
(11, 2, '理发', 0, NULL, NULL, NULL),
(12, 2, '资讯', 0, NULL, NULL, NULL),
(13, 2, '旅游', 0, NULL, NULL, NULL),
(14, 2, '房产', 0, NULL, NULL, NULL),
(15, 2, '美甲', 0, NULL, NULL, NULL),
(16, 2, '物业', 0, NULL, NULL, NULL),
(17, 2, '珠宝', 0, NULL, NULL, NULL),
(18, 2, '美容', 0, NULL, NULL, NULL),
(19, 2, '学校', 0, NULL, NULL, NULL),
(20, 2, '母婴', 0, NULL, NULL, NULL),
(21, 2, '书店', 0, NULL, NULL, NULL),
(22, 2, '摄影', 0, NULL, NULL, NULL),
(23, 2, '其他', 0, NULL, NULL, NULL),
(24, 1, '其他', 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `template_wxa`
--

CREATE TABLE `template_wxa` (
  `id` int(10) UNSIGNED NOT NULL,
  `template_id` int(11) NOT NULL DEFAULT '0' COMMENT '模版',
  `templateid` int(11) NOT NULL DEFAULT '0' COMMENT '小程序模版id',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '主题色',
  `info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '介绍',
  `ext_json` text COLLATE utf8mb4_unicode_ci COMMENT 'ext_json',
  `rank` tinyint(4) NOT NULL DEFAULT '0' COMMENT '排序',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='小程序模版';

--
-- 转存表中的数据 `template_wxa`
--

INSERT INTO `template_wxa` (`id`, `template_id`, `templateid`, `name`, `color`, `info`, `ext_json`, `rank`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 61, '微站', '#FC4C4F', '微站小程序', '{}', 0, NULL, NULL, NULL),
(2, 2, 61, '微站', '#7870FF', '微站小程序', '{}', 0, NULL, NULL, NULL),
(3, 2, 61, '微站', '#39CBAC', '微站小程序', '{}', 0, NULL, NULL, NULL),
(4, 2, 61, '微站', '#FE66A3', '微站小程序', '{}', 0, NULL, NULL, NULL),
(5, 2, 61, '微站', '#FE8A48', '微站小程序', '{}', 0, NULL, NULL, NULL),
(6, 2, 61, '微站', '#FAC632', '微站小程序', '{}', 0, NULL, NULL, NULL),
(7, 2, 61, '微站', '#3D94FF', '微站小程序', '{}', 0, NULL, NULL, NULL),
(8, 1, 61, '零售', '#3D94FF', '零售小程序', '{\"ext\":{\"color\":\"#3D94FF\"},\"window\":{\"backgroundColor\":\"#3D94FF\",\"navigationBarBackgroundColor\":\"#3D94FF\",\"navigationBarTextStyle\":\"white\",\"navigationStyle\":\"default\",\"onReachBottomDistance\":100},\"tabBar\":{\"color\":\"#cfcfcf\",\"borderStyle\":\"white\",\"backgroundColor\":\"#fff\",\"selectedColor\":\"#a0a0a0\",\"list\":[{\"pagePath\":\"pages/index/index\",\"iconPath\":\"theme/default/home.png\",\"selectedIconPath\":\"theme/default/home@selected.png\",\"text\":\"主页\"},{\"pagePath\":\"pages/shop/cart/index\",\"iconPath\":\"theme/default/cart.png\",\"selectedIconPath\":\"theme/default/cart@selected.png\",\"text\":\"购物车\"},{\"pagePath\":\"pages/shop/user/index\",\"iconPath\":\"theme/default/my.png\",\"selectedIconPath\":\"theme/default/my@selected.png\",\"text\":\"我的\"}]}}', 0, NULL, NULL, NULL),
(9, 1, 61, '零售', '#FC4C4F', '零售小程序', '{\"ext\":{\"color\":\"#fc4c4f\"},\"window\":{\"backgroundColor\":\"#fc4c4f\",\"navigationBarBackgroundColor\":\"#fc4c4f\",\"navigationBarTextStyle\":\"white\",\"navigationStyle\":\"default\",\"onReachBottomDistance\":100},\"tabBar\":{\"color\":\"#cfcfcf\",\"borderStyle\":\"white\",\"backgroundColor\":\"#fff\",\"selectedColor\":\"#a0a0a0\",\"list\":[{\"pagePath\":\"pages/index/index\",\"iconPath\":\"theme/default/home.png\",\"selectedIconPath\":\"theme/default/home@selected.png\",\"text\":\"主页\"},{\"pagePath\":\"pages/shop/cart/index\",\"iconPath\":\"theme/default/cart.png\",\"selectedIconPath\":\"theme/default/cart@selected.png\",\"text\":\"购物车\"},{\"pagePath\":\"pages/shop/user/index\",\"iconPath\":\"theme/default/my.png\",\"selectedIconPath\":\"theme/default/my@selected.png\",\"text\":\"我的\"}]}}', 0, NULL, NULL, NULL),
(10, 1, 61, '零售', '#7870FF', '零售小程序', '{\"ext\":{\"color\":\"#7870FF\"},\"window\":{\"backgroundColor\":\"#7870FF\",\"navigationBarBackgroundColor\":\"#7870FF\",\"navigationBarTextStyle\":\"white\",\"navigationStyle\":\"default\",\"onReachBottomDistance\":100},\"tabBar\":{\"color\":\"#cfcfcf\",\"borderStyle\":\"white\",\"backgroundColor\":\"#fff\",\"selectedColor\":\"#a0a0a0\",\"list\":[{\"pagePath\":\"pages/index/index\",\"iconPath\":\"theme/default/home.png\",\"selectedIconPath\":\"theme/default/home@selected.png\",\"text\":\"主页\"},{\"pagePath\":\"pages/shop/cart/index\",\"iconPath\":\"theme/default/cart.png\",\"selectedIconPath\":\"theme/default/cart@selected.png\",\"text\":\"购物车\"},{\"pagePath\":\"pages/shop/user/index\",\"iconPath\":\"theme/default/my.png\",\"selectedIconPath\":\"theme/default/my@selected.png\",\"text\":\"我的\"}]}}', 0, NULL, NULL, NULL),
(11, 1, 61, '零售', '#39CBAC', '零售小程序', '{\"ext\":{\"color\":\"#39CBAC\"},\"window\":{\"backgroundColor\":\"#39CBAC\",\"navigationBarBackgroundColor\":\"#39CBAC\",\"navigationBarTextStyle\":\"white\",\"navigationStyle\":\"default\",\"onReachBottomDistance\":100},\"tabBar\":{\"color\":\"#cfcfcf\",\"borderStyle\":\"white\",\"backgroundColor\":\"#fff\",\"selectedColor\":\"#a0a0a0\",\"list\":[{\"pagePath\":\"pages/index/index\",\"iconPath\":\"theme/default/home.png\",\"selectedIconPath\":\"theme/default/home@selected.png\",\"text\":\"主页\"},{\"pagePath\":\"pages/shop/cart/index\",\"iconPath\":\"theme/default/cart.png\",\"selectedIconPath\":\"theme/default/cart@selected.png\",\"text\":\"购物车\"},{\"pagePath\":\"pages/shop/user/index\",\"iconPath\":\"theme/default/my.png\",\"selectedIconPath\":\"theme/default/my@selected.png\",\"text\":\"我的\"}]}}', 0, NULL, NULL, NULL),
(12, 1, 61, '零售', '#FE66A3', '零售小程序', '{\"ext\":{\"color\":\"#FE66A3\"},\"window\":{\"backgroundColor\":\"#FE66A3\",\"navigationBarBackgroundColor\":\"#FE66A3\",\"navigationBarTextStyle\":\"white\",\"navigationStyle\":\"default\",\"onReachBottomDistance\":100},\"tabBar\":{\"color\":\"#cfcfcf\",\"borderStyle\":\"white\",\"backgroundColor\":\"#fff\",\"selectedColor\":\"#a0a0a0\",\"list\":[{\"pagePath\":\"pages/index/index\",\"iconPath\":\"theme/default/home.png\",\"selectedIconPath\":\"theme/default/home@selected.png\",\"text\":\"主页\"},{\"pagePath\":\"pages/shop/cart/index\",\"iconPath\":\"theme/default/cart.png\",\"selectedIconPath\":\"theme/default/cart@selected.png\",\"text\":\"购物车\"},{\"pagePath\":\"pages/shop/user/index\",\"iconPath\":\"theme/default/my.png\",\"selectedIconPath\":\"theme/default/my@selected.png\",\"text\":\"我的\"}]}}', 0, NULL, NULL, NULL),
(13, 1, 61, '零售', '#FE8A48', '零售小程序', '{\"ext\":{\"color\":\"#FE8A48\"},\"window\":{\"backgroundColor\":\"#FE8A48\",\"navigationBarBackgroundColor\":\"#FE8A48\",\"navigationBarTextStyle\":\"white\",\"navigationStyle\":\"default\",\"onReachBottomDistance\":100},\"tabBar\":{\"color\":\"#cfcfcf\",\"borderStyle\":\"white\",\"backgroundColor\":\"#fff\",\"selectedColor\":\"#a0a0a0\",\"list\":[{\"pagePath\":\"pages/index/index\",\"iconPath\":\"theme/default/home.png\",\"selectedIconPath\":\"theme/default/home@selected.png\",\"text\":\"主页\"},{\"pagePath\":\"pages/shop/cart/index\",\"iconPath\":\"theme/default/cart.png\",\"selectedIconPath\":\"theme/default/cart@selected.png\",\"text\":\"购物车\"},{\"pagePath\":\"pages/shop/user/index\",\"iconPath\":\"theme/default/my.png\",\"selectedIconPath\":\"theme/default/my@selected.png\",\"text\":\"我的\"}]}}', 0, NULL, NULL, NULL),
(14, 1, 61, '零售', '#FAC632', '零售小程序', '{\"ext\":{\"color\":\"#FAC632\"},\"window\":{\"backgroundColor\":\"#FAC632\",\"navigationBarBackgroundColor\":\"#FAC632\",\"navigationBarTextStyle\":\"white\",\"navigationStyle\":\"default\",\"onReachBottomDistance\":100},\"tabBar\":{\"color\":\"#cfcfcf\",\"borderStyle\":\"white\",\"backgroundColor\":\"#fff\",\"selectedColor\":\"#a0a0a0\",\"list\":[{\"pagePath\":\"pages/index/index\",\"iconPath\":\"theme/default/home.png\",\"selectedIconPath\":\"theme/default/home@selected.png\",\"text\":\"主页\"},{\"pagePath\":\"pages/shop/cart/index\",\"iconPath\":\"theme/default/cart.png\",\"selectedIconPath\":\"theme/default/cart@selected.png\",\"text\":\"购物车\"},{\"pagePath\":\"pages/shop/user/index\",\"iconPath\":\"theme/default/my.png\",\"selectedIconPath\":\"theme/default/my@selected.png\",\"text\":\"我的\"}]}}', 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `trade`
--

CREATE TABLE `trade` (
  `id` int(10) UNSIGNED NOT NULL,
  `wxa_id` int(11) NOT NULL DEFAULT '0' COMMENT '小程序',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户',
  `tradeid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '交易单号',
  `money` float NOT NULL DEFAULT '0' COMMENT '金额',
  `payment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '支付方式',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '支付状态',
  `notify_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '回调链接',
  `notify_data` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '回调信息',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0表示普通商城交易，1买单交易',
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='交易';

-- --------------------------------------------------------

--
-- 表的结构 `ump`
--

CREATE TABLE `ump` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名称',
  `sole` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '唯一标识',
  `price` float NOT NULL DEFAULT '0' COMMENT '价格',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='营销';

-- --------------------------------------------------------

--
-- 表的结构 `ump_trade`
--

CREATE TABLE `ump_trade` (
  `id` int(11) UNSIGNED NOT NULL,
  `custom_id` int(11) NOT NULL DEFAULT '0' COMMENT '客户id',
  `app_id` int(11) NOT NULL DEFAULT '0' COMMENT 'app_id',
  `ump_id` int(11) NOT NULL DEFAULT '0' COMMENT 'ump_id',
  `tradeid` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '交易单号',
  `money` float NOT NULL DEFAULT '0' COMMENT '金额',
  `payment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '支付方式',
  `pay_status` int(11) NOT NULL DEFAULT '0' COMMENT '1:已支付0:未支付',
  `years` int(11) NOT NULL DEFAULT '0' COMMENT '几年',
  `remark` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='营销';

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `wxa_id` int(11) NOT NULL DEFAULT '0' COMMENT '小程序',
  `openId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'openid',
  `nickName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '昵称',
  `gender` tinyint(1) NOT NULL DEFAULT '0' COMMENT '性别，1:男，2:女',
  `language` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '语言',
  `city` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `province` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `country` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `avatarUrl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '头像链接',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户';

-- --------------------------------------------------------

--
-- 表的结构 `verify`
--

CREATE TABLE `verify` (
  `id` int(10) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'uuid',
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '值',
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ip',
  `phone` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机号',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态，0:未使用，1:已核销',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='验证码';

--
-- 转存表中的数据 `verify`
--

INSERT INTO `verify` (`id`, `uuid`, `value`, `ip`, `phone`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '347c0aeb-e59a-4473-8f73-2259f748629b', 'ieph', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 02:18:04', '2018-09-05 02:18:04', NULL),
(2, '986e4e3e-0580-46d5-abbf-c298b59745db', 'zrws', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 02:18:17', '2018-09-05 02:18:17', NULL),
(3, '41bc129d-ce80-42ee-8972-3de5bd4954b1', 'pwqz', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 02:19:01', '2018-09-05 02:19:01', NULL),
(4, '72cda8fc-2859-43a3-805c-eaabcff82b6b', '7cj1', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 02:19:12', '2018-09-05 02:19:12', NULL),
(5, '2ec6bd28-cccf-4dc8-9abb-f5f7cf8233f5', 'gsqy', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 02:19:27', '2018-09-05 02:19:27', NULL),
(6, '485ae9de-20cb-47de-acf8-ff7c01cd6432', '8aua', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 02:21:42', '2018-09-05 02:21:42', NULL),
(7, 'c98d35e6-3be0-41ef-a4c6-11163d55379c', 'aez7', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 02:21:46', '2018-09-05 02:21:46', NULL),
(8, 'fb7c2a4e-f999-4043-a8e6-28a5c8c0e57d', 'pdjv', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 02:22:41', '2018-09-05 02:22:41', NULL),
(9, 'e28032db-7bff-4222-b32e-b7b3a3f7cffa', 'ipw7', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 02:22:42', '2018-09-05 02:22:42', NULL),
(10, '8e85642f-b334-488e-a6d8-cb9c3bdef028', '3icg', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 02:22:42', '2018-09-05 02:22:42', NULL),
(11, '4ff82b91-1bb3-4a35-be4c-3766b9860cfd', 'n6nh', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 02:22:43', '2018-09-05 02:22:43', NULL),
(12, '2d2c395d-8be5-4e96-aa96-aedf5e837c9a', 'apac', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 02:22:44', '2018-09-05 02:22:44', NULL),
(13, '170dfc5a-3ac1-4191-a0a7-4e0105cdce9f', '2iwf', '::ffff:127.0.0.1', NULL, 1, '2018-09-05 02:26:58', '2018-09-05 02:27:36', NULL),
(14, 'a0e1c018-25d8-4094-abd7-eec8ab12e472', 'urj2', '::ffff:127.0.0.1', NULL, 1, '2018-09-05 02:27:36', '2018-09-05 02:31:36', NULL),
(15, '409fe347-69d6-44d4-a028-22842d14354c', 'xx8y', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:42:52', '2018-09-05 06:42:52', NULL),
(16, '0f50ffc0-5c4a-45e6-9536-6ec4a338ffec', '6nnm', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:43:05', '2018-09-05 06:43:05', NULL),
(17, 'e4803055-2dbf-4f96-9afe-83f88c11bc86', 'ib7a', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:43:12', '2018-09-05 06:43:12', NULL),
(18, 'b15f3763-651b-46fc-bd32-ef8ba5234e83', '9dgm', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:43:14', '2018-09-05 06:43:14', NULL),
(19, '07282e69-acb4-4561-b4ad-ed47a9a918e2', '48tx', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:43:17', '2018-09-05 06:43:17', NULL),
(20, '1323aa2a-248a-43d4-84de-a65255ea0d5a', 'nvot', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:43:20', '2018-09-05 06:43:20', NULL),
(21, 'e34f6342-0044-43a7-b757-62f45f8000d9', 'ngxt', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:43:31', '2018-09-05 06:43:31', NULL),
(22, '817dee40-8605-4251-bcd6-b030738ca0e3', 'pa4g', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:43:34', '2018-09-05 06:43:34', NULL),
(23, 'a3b398c1-b503-4aa0-b0b4-8456bcf2e91f', 'uqdz', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:46:35', '2018-09-05 06:46:35', NULL),
(24, 'ce9695d7-feca-42c2-b177-e5f6ec44cc3b', 'u9mk', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:46:55', '2018-09-05 06:46:55', NULL),
(25, '44168974-91a2-41d1-9ad0-2bf9e0089ecb', 'vd1k', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:46:59', '2018-09-05 06:46:59', NULL),
(26, '070f0605-dd2c-4e39-9de6-b29953ea299b', 'nfq1', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:47:08', '2018-09-05 06:47:08', NULL),
(27, '0f576961-a1f9-470a-b183-d5170d7612db', 'rgfo', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:47:08', '2018-09-05 06:47:08', NULL),
(28, 'ddf9d64a-01b5-4130-b7b1-0a3f63ddc3bc', 'zr2g', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:47:09', '2018-09-05 06:47:09', NULL),
(29, 'cb3f16a5-2e5d-4eb5-b519-33aae8c4d638', 'nalj', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:47:10', '2018-09-05 06:47:10', NULL),
(30, '4cd7df56-01c7-436b-96fe-11d51c769975', 'tqvt', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:47:11', '2018-09-05 06:47:11', NULL),
(31, 'b4a8f9fb-85a6-4e5f-88ef-ec1d82357a2e', '8nfd', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:47:34', '2018-09-05 06:47:34', NULL),
(32, 'be9d9ad6-e37f-41f0-9930-7a45bfe4845a', 'pyrr', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:47:35', '2018-09-05 06:47:35', NULL),
(33, '7ef07cde-bb61-4f1c-b886-719725b85b56', 'c2jd', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:47:36', '2018-09-05 06:47:36', NULL),
(34, 'dc7e1811-08bd-4c33-8f49-db2d0ae73cef', '5mt7', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:47:37', '2018-09-05 06:47:37', NULL),
(35, '08b93e1c-e1ba-40ef-b0c3-e5e7d5d16a4f', 'priy', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:47:37', '2018-09-05 06:47:37', NULL),
(36, '04f485a1-793f-46ed-b35e-c90a8a852d6f', 'e4if', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:47:38', '2018-09-05 06:47:38', NULL),
(37, '6f0e4528-637b-4295-a0ce-a70c34fdb6e2', 'jpmq', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:47:38', '2018-09-05 06:47:38', NULL),
(38, '84905e69-19e4-40db-9296-6b3104c39c5c', 'xrgw', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:47:39', '2018-09-05 06:47:39', NULL),
(39, '835533f8-2023-4a30-b8ca-480dcd2328b0', 'cl8d', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:47:55', '2018-09-05 06:47:55', NULL),
(40, '7542f8a9-013c-41e7-9f79-2eaa86930181', 'l30y', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:48:32', '2018-09-05 06:48:32', NULL),
(41, 'ececab1e-8674-4c39-8349-bec65caa9b66', '7ckf', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:48:36', '2018-09-05 06:48:36', NULL),
(42, 'e3cc370a-26a4-4832-a6c2-9972522a2656', 'fp53', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:49:02', '2018-09-05 06:49:02', NULL),
(43, '14410272-7352-4245-be89-79ff517e4a37', '676v', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:49:38', '2018-09-05 06:49:38', NULL),
(44, 'dd527cb4-a202-4a2b-82d4-828ea3e083cf', '28h1', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:50:47', '2018-09-05 06:50:47', NULL),
(45, '2d70ec58-f94d-4fe5-9d38-521b813e6c76', '5ulz', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:51:02', '2018-09-05 06:51:02', NULL),
(46, '910db54b-7068-4143-a240-7f54f5a22c2a', 'xi0n', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:51:43', '2018-09-05 06:51:43', NULL),
(47, '6b49703e-10fd-4fc0-92c8-e73340f38055', 'byae', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:52:10', '2018-09-05 06:52:10', NULL),
(48, '3b55bb0f-6501-4d52-b9be-0e703854813c', 'tc66', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:53:02', '2018-09-05 06:53:02', NULL),
(49, '302220dd-8266-4be4-b776-b0ae818529b6', 'dolz', '::ffff:127.0.0.1', NULL, 0, '2018-09-05 06:53:35', '2018-09-05 06:53:35', NULL),
(50, '7c735f63-7ca1-45b0-8f6f-d6aad688db92', 'hyds', '::ffff:127.0.0.1', NULL, 1, '2018-09-06 06:54:43', '2018-09-06 06:56:46', NULL),
(51, 'da64ae6d-70b6-4629-9537-14459c163047', 'jowe', '::ffff:127.0.0.1', NULL, 1, '2018-09-06 06:57:16', '2018-09-06 06:58:44', NULL),
(52, 'edf8135b-95b5-4d45-8997-ed0828b36e18', 'kwnz', '::ffff:127.0.0.1', NULL, 0, '2018-09-06 07:01:53', '2018-09-06 07:01:53', NULL),
(53, 'a5cc98f0-1b30-4790-997a-68ce949d7fbe', 'eymz', '::ffff:127.0.0.1', NULL, 1, '2018-09-06 07:03:08', '2018-09-06 07:03:25', NULL),
(54, 'd3409b39-fe70-4d28-b8bf-851a1f8060c7', '1wqz', '::ffff:127.0.0.1', NULL, 0, '2018-09-06 07:03:28', '2018-09-06 07:03:28', NULL),
(55, '93445ff9-537e-443f-b30c-e36813afa950', 'qm6x', '::ffff:127.0.0.1', NULL, 0, '2018-09-06 07:03:59', '2018-09-06 07:03:59', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `wxa`
--

CREATE TABLE `wxa` (
  `id` int(10) UNSIGNED NOT NULL,
  `app_id` int(11) NOT NULL DEFAULT '0' COMMENT '应用',
  `authorizer_appid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'authorizer_appid',
  `authorizer_access_token` text COLLATE utf8mb4_unicode_ci COMMENT 'authorizer_access_token',
  `expires_in` int(11) DEFAULT NULL COMMENT '有效期',
  `expires_at` bigint(20) DEFAULT NULL COMMENT '失效时间',
  `authorizer_refresh_token` text COLLATE utf8mb4_unicode_ci COMMENT 'authorizer_refresh_token',
  `nick_name` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '授权方昵称',
  `head_img` text COLLATE utf8mb4_unicode_ci COMMENT '授权方头像',
  `user_name` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '授权方公众号的原始ID',
  `principal_name` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '公众号的主体名称',
  `open_pay` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否开通微信支付功能',
  `open_shake` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否开通微信摇一摇功能',
  `open_scan` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否开通微信扫商品功能',
  `open_card` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否开通微信卡券功能',
  `open_store` int(11) NOT NULL DEFAULT '0' COMMENT '是否开通微信门店功能',
  `signature` text COLLATE utf8mb4_unicode_ci COMMENT '签名',
  `func_info` text COLLATE utf8mb4_unicode_ci COMMENT '授权给开发者的权限集列表',
  `verify_type_info` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '授权方认证类型，-1代表未认证，0代表微信认证',
  `request_domain` text COLLATE utf8mb4_unicode_ci COMMENT 'request_domain',
  `wsrequest_domain` text COLLATE utf8mb4_unicode_ci COMMENT 'wsrequest_domain',
  `upload_domain` text COLLATE utf8mb4_unicode_ci COMMENT 'upload_domain',
  `download_domain` text COLLATE utf8mb4_unicode_ci COMMENT 'download_domain',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `qrcode_url` text COLLATE utf8mb4_unicode_ci COMMENT '二维码图片的URL',
  `authorized` tinyint(1) NOT NULL DEFAULT '1' COMMENT '授权状态',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='小程序';

--
-- 转存表中的数据 `wxa`
--

INSERT INTO `wxa` (`id`, `app_id`, `authorizer_appid`, `authorizer_access_token`, `expires_in`, `expires_at`, `authorizer_refresh_token`, `nick_name`, `head_img`, `user_name`, `principal_name`, `open_pay`, `open_shake`, `open_scan`, `open_card`, `open_store`, `signature`, `func_info`, `verify_type_info`, `request_domain`, `wsrequest_domain`, `upload_domain`, `download_domain`, `status`, `qrcode_url`, `authorized`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'wx9ca8f72a528da0db', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `wxa_audit`
--

CREATE TABLE `wxa_audit` (
  `id` int(10) UNSIGNED NOT NULL,
  `wxa_id` int(11) NOT NULL DEFAULT '0' COMMENT '小程序',
  `template_wxa_id` int(11) NOT NULL DEFAULT '0' COMMENT '小程序模版',
  `templateid` int(11) NOT NULL DEFAULT '0' COMMENT '小程序模版id',
  `auditid` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '审核ID',
  `audit_item` text COLLATE utf8mb4_unicode_ci COMMENT '审核数据',
  `ext_json` text COLLATE utf8mb4_unicode_ci COMMENT 'ext_json',
  `user_version` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '版本',
  `user_desc` text COLLATE utf8mb4_unicode_ci COMMENT '描述',
  `release` tinyint(1) NOT NULL DEFAULT '0' COMMENT '发布状态',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='小程序审核';

-- --------------------------------------------------------

--
-- 表的结构 `wxa_log`
--

CREATE TABLE `wxa_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `wxa_id` int(11) NOT NULL DEFAULT '0' COMMENT '小程序',
  `info` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '信息',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='小程序日志';

-- --------------------------------------------------------

--
-- 表的结构 `wxa_msg`
--

CREATE TABLE `wxa_msg` (
  `id` int(10) UNSIGNED NOT NULL,
  `wxa_id` int(11) NOT NULL DEFAULT '0' COMMENT '小程序',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户',
  `ToUserName` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '小程序user_name',
  `FromUserName` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户openid',
  `unread` tinyint(1) NOT NULL DEFAULT '0' COMMENT '未读消息',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='小程序客服消息';

-- --------------------------------------------------------

--
-- 表的结构 `wxa_pay`
--

CREATE TABLE `wxa_pay` (
  `id` int(10) UNSIGNED NOT NULL,
  `wxa_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '小程序',
  `mchid` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '商户号',
  `key` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '商户秘钥',
  `pfx` text COLLATE utf8mb4_unicode_ci COMMENT '商户支付证书',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='小程序微信支付';

-- --------------------------------------------------------

--
-- 表的结构 `wxa_tester`
--

CREATE TABLE `wxa_tester` (
  `id` int(10) UNSIGNED NOT NULL,
  `wxa_id` int(11) NOT NULL DEFAULT '0' COMMENT '小程序',
  `wechatid` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '微信号',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='小程序体验者';

-- --------------------------------------------------------

--
-- 表的结构 `wxa_tplmsg`
--

CREATE TABLE `wxa_tplmsg` (
  `id` int(11) UNSIGNED NOT NULL,
  `wxa_id` int(11) NOT NULL DEFAULT '0',
  `title_id` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '模板标题id',
  `keyword_id` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '关键词id',
  `tpl_id` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '模板id',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='小程序模版消息' ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- 表的结构 `wx_user`
--

CREATE TABLE `wx_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `custom_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `openid` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户的标识，对当前公众号唯一',
  `nickname` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户的昵称',
  `sex` int(11) NOT NULL DEFAULT '0' COMMENT '1:男2:女0:未知',
  `city` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户所在城市',
  `country` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户所在国家',
  `province` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户所在省份',
  `language` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '语言',
  `headimgurl` text COLLATE utf8mb4_unicode_ci COMMENT '用户头像',
  `unionid` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '统一用户',
  `subscribe` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:关注0:取消',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='微信公众号用户';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_log`
--
ALTER TABLE `admin_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `file_id` (`img_url`(191));

--
-- Indexes for table `analysis_daily_summary`
--
ALTER TABLE `analysis_daily_summary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `analysis_product_effect_summary`
--
ALTER TABLE `analysis_product_effect_summary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `analysis_product_summary`
--
ALTER TABLE `analysis_product_summary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `analysis_product_user_summary`
--
ALTER TABLE `analysis_product_user_summary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `analysis_wxa_daily_summary`
--
ALTER TABLE `analysis_wxa_daily_summary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app`
--
ALTER TABLE `app`
  ADD PRIMARY KEY (`id`),
  ADD KEY `custom_id` (`custom_id`),
  ADD KEY `template_id` (`template_id`),
  ADD KEY `template_industry_id` (`template_industry_id`);

--
-- Indexes for table `app_address`
--
ALTER TABLE `app_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `app_id` (`app_id`);

--
-- Indexes for table `app_log`
--
ALTER TABLE `app_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_trade`
--
ALTER TABLE `app_trade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_ump`
--
ALTER TABLE `app_ump`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `component_access_token`
--
ALTER TABLE `component_access_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `component_verify_ticket`
--
ALTER TABLE `component_verify_ticket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom`
--
ALTER TABLE `custom`
  ADD PRIMARY KEY (`id`),
  ADD KEY `avater_id` (`avater_url`(191)),
  ADD KEY `phone` (`phone`(191));

--
-- Indexes for table `custom_group`
--
ALTER TABLE `custom_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_log`
--
ALTER TABLE `custom_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `custom_id` (`custom_id`),
  ADD KEY `app_id` (`app_id`);

--
-- Indexes for table `kuaidi`
--
ALTER TABLE `kuaidi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_coupon`
--
ALTER TABLE `shop_coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_deliveryfee`
--
ALTER TABLE `shop_deliveryfee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_deliveryfee_sku`
--
ALTER TABLE `shop_deliveryfee_sku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_deliveryfee_sku_address`
--
ALTER TABLE `shop_deliveryfee_sku_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_fullcut`
--
ALTER TABLE `shop_fullcut`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_kuaidi`
--
ALTER TABLE `shop_kuaidi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_order`
--
ALTER TABLE `shop_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_order_contact`
--
ALTER TABLE `shop_order_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_order_detail`
--
ALTER TABLE `shop_order_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_order_kuaidi`
--
ALTER TABLE `shop_order_kuaidi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_order_pintuan`
--
ALTER TABLE `shop_order_pintuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_order_service`
--
ALTER TABLE `shop_order_service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_order_verify`
--
ALTER TABLE `shop_order_verify`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_product`
--
ALTER TABLE `shop_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_productgroup`
--
ALTER TABLE `shop_productgroup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_productgroup_product`
--
ALTER TABLE `shop_productgroup_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_productlabel`
--
ALTER TABLE `shop_productlabel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_productlabel_product`
--
ALTER TABLE `shop_productlabel_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_productsku`
--
ALTER TABLE `shop_productsku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_product_album`
--
ALTER TABLE `shop_product_album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_product_pintuan_set`
--
ALTER TABLE `shop_product_pintuan_set`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_region`
--
ALTER TABLE `shop_region`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `shop_service`
--
ALTER TABLE `shop_service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_sku`
--
ALTER TABLE `shop_sku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_user_collection`
--
ALTER TABLE `shop_user_collection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_user_coupon`
--
ALTER TABLE `shop_user_coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`id`),
  ADD KEY `file_id` (`img_url`(191));

--
-- Indexes for table `template_industry`
--
ALTER TABLE `template_industry`
  ADD PRIMARY KEY (`id`),
  ADD KEY `template_id` (`template_id`);

--
-- Indexes for table `template_wxa`
--
ALTER TABLE `template_wxa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `template_id` (`template_id`),
  ADD KEY `templateid` (`templateid`);

--
-- Indexes for table `trade`
--
ALTER TABLE `trade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ump`
--
ALTER TABLE `ump`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ump_trade`
--
ALTER TABLE `ump_trade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wxa_id` (`wxa_id`),
  ADD KEY `openId` (`openId`(191));

--
-- Indexes for table `verify`
--
ALTER TABLE `verify`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uuid` (`uuid`(42));

--
-- Indexes for table `wxa`
--
ALTER TABLE `wxa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `app_id` (`app_id`),
  ADD KEY `authorizer_appid` (`authorizer_appid`(24));

--
-- Indexes for table `wxa_audit`
--
ALTER TABLE `wxa_audit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wxa_id` (`wxa_id`),
  ADD KEY `templated_wxa_id` (`template_wxa_id`),
  ADD KEY `auditid` (`auditid`(24));

--
-- Indexes for table `wxa_log`
--
ALTER TABLE `wxa_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wxa_msg`
--
ALTER TABLE `wxa_msg`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wxa_pay`
--
ALTER TABLE `wxa_pay`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wxa_tester`
--
ALTER TABLE `wxa_tester`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wxa_id` (`wxa_id`);

--
-- Indexes for table `wxa_tplmsg`
--
ALTER TABLE `wxa_tplmsg`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wx_user`
--
ALTER TABLE `wx_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `openid` (`openid`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `admin_log`
--
ALTER TABLE `admin_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- 使用表AUTO_INCREMENT `analysis_daily_summary`
--
ALTER TABLE `analysis_daily_summary`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `analysis_product_effect_summary`
--
ALTER TABLE `analysis_product_effect_summary`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `analysis_product_summary`
--
ALTER TABLE `analysis_product_summary`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `analysis_product_user_summary`
--
ALTER TABLE `analysis_product_user_summary`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `analysis_wxa_daily_summary`
--
ALTER TABLE `analysis_wxa_daily_summary`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `app`
--
ALTER TABLE `app`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `app_address`
--
ALTER TABLE `app_address`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `app_log`
--
ALTER TABLE `app_log`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `app_trade`
--
ALTER TABLE `app_trade`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `app_ump`
--
ALTER TABLE `app_ump`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `component_access_token`
--
ALTER TABLE `component_access_token`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `component_verify_ticket`
--
ALTER TABLE `component_verify_ticket`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `custom`
--
ALTER TABLE `custom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `custom_group`
--
ALTER TABLE `custom_group`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `custom_log`
--
ALTER TABLE `custom_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- 使用表AUTO_INCREMENT `file`
--
ALTER TABLE `file`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- 使用表AUTO_INCREMENT `kuaidi`
--
ALTER TABLE `kuaidi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop`
--
ALTER TABLE `shop`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `shop_coupon`
--
ALTER TABLE `shop_coupon`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop_deliveryfee`
--
ALTER TABLE `shop_deliveryfee`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop_deliveryfee_sku`
--
ALTER TABLE `shop_deliveryfee_sku`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop_deliveryfee_sku_address`
--
ALTER TABLE `shop_deliveryfee_sku_address`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop_fullcut`
--
ALTER TABLE `shop_fullcut`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop_kuaidi`
--
ALTER TABLE `shop_kuaidi`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop_order`
--
ALTER TABLE `shop_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop_order_contact`
--
ALTER TABLE `shop_order_contact`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop_order_detail`
--
ALTER TABLE `shop_order_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop_order_kuaidi`
--
ALTER TABLE `shop_order_kuaidi`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop_order_pintuan`
--
ALTER TABLE `shop_order_pintuan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop_order_service`
--
ALTER TABLE `shop_order_service`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop_order_verify`
--
ALTER TABLE `shop_order_verify`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop_product`
--
ALTER TABLE `shop_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- 使用表AUTO_INCREMENT `shop_productgroup`
--
ALTER TABLE `shop_productgroup`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `shop_productgroup_product`
--
ALTER TABLE `shop_productgroup_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- 使用表AUTO_INCREMENT `shop_productlabel`
--
ALTER TABLE `shop_productlabel`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop_productlabel_product`
--
ALTER TABLE `shop_productlabel_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop_productsku`
--
ALTER TABLE `shop_productsku`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop_product_album`
--
ALTER TABLE `shop_product_album`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- 使用表AUTO_INCREMENT `shop_product_pintuan_set`
--
ALTER TABLE `shop_product_pintuan_set`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop_service`
--
ALTER TABLE `shop_service`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `shop_sku`
--
ALTER TABLE `shop_sku`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop_user_collection`
--
ALTER TABLE `shop_user_collection`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `shop_user_coupon`
--
ALTER TABLE `shop_user_coupon`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `template`
--
ALTER TABLE `template`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `template_industry`
--
ALTER TABLE `template_industry`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- 使用表AUTO_INCREMENT `template_wxa`
--
ALTER TABLE `template_wxa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- 使用表AUTO_INCREMENT `trade`
--
ALTER TABLE `trade`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `ump`
--
ALTER TABLE `ump`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `ump_trade`
--
ALTER TABLE `ump_trade`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `verify`
--
ALTER TABLE `verify`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- 使用表AUTO_INCREMENT `wxa`
--
ALTER TABLE `wxa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `wxa_audit`
--
ALTER TABLE `wxa_audit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `wxa_log`
--
ALTER TABLE `wxa_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `wxa_msg`
--
ALTER TABLE `wxa_msg`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `wxa_pay`
--
ALTER TABLE `wxa_pay`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `wxa_tester`
--
ALTER TABLE `wxa_tester`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `wxa_tplmsg`
--
ALTER TABLE `wxa_tplmsg`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `wx_user`
--
ALTER TABLE `wx_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
