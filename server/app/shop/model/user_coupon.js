const coupon = require("./coupon");

module.exports = doodoo.bookshelf.Model.extend({
    tableName: "shop_user_coupon",
    hasTimestamps: true,
    coupon: function() {
        return this.belongsTo(coupon, "coupon_id");
    }
});
