const axios = require("axios");
const moment = require("moment");
const url =
    "xxx";

module.exports = function ding(title, text) {
    try {
        return axios({
            url: url,
            method: "post",
            timeout: 3000,
            headers: {
                "Content-Type": "application/json"
            },
            data: {
                msgtype: "markdown",
                markdown: {
                    title: title,
                    text: `#### ${title} ####\n${text}\n#### 时间：${moment().format(
                        "YYYY-MM-DD HH:mm:ss"
                    )} ####`
                }
            }
        });
    } catch (error) {
        console.log(error);
        console.error("Ding Network fail");
    }
};
