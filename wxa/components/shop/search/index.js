Component({
    properties: {
        props: String
    },
    data:{
        text: '搜索',
        search: ''
    },
    attached: function() {
        this.setData(
            Object.assign(
                this.data,
                this.data.props ? JSON.parse(this.data.props) : {}
            )
        );
    },
    methods: {
        goSearch() {
            console.log('应该去搜索历史页面');
            // wx.navigateTo({
            //     url: "../../pages/plugin/search/index"
            // });
        },
        bindconfirm(e) {
            setTimeout(() => {
                this.setData({
                    text: ''
                })
            }, 200)
            wx.navigateTo({
                url: `/pages/shop/product/product-list/index?text=${e.detail.value}`
            });
        }
    }
});